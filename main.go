package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"time"

	"github.com/gammazero/workerpool"
	"golang.org/x/net/proxy"
)

var (
	err        error
	outfile    = "out.txt"
	filenlist  string
	pathlist   string
	domainlist string
	ualist     string
	proxylist  string
	uas        []string
	proxys     []string
	filenames  []string
	paths      []string
	domains    []string
	workers    int
	mode       int
)

func init() {
	rand.Seed(time.Now().Unix())

	flag.StringVar(&filenlist, "files", "", "список имён файлов")
	flag.StringVar(&pathlist, "path", "", "список директорий")
	flag.StringVar(&domainlist, "domains", "", "список доменов")
	flag.StringVar(&ualist, "ua", "", "список юзер агентов")
	flag.StringVar(&proxylist, "proxy", "", "список прокси")
	flag.IntVar(&workers, "w", 100, "количество потоков")
	flag.IntVar(&mode, "mode", 1, "mode")
	flag.Parse()
	if filenlist == "" || pathlist == "" || domainlist == "" || ualist == "" || proxylist == "" || workers < 1 {
		fmt.Println("Не указаны необходимые параметры")
		os.Exit(1)
	}
	switch mode {
	case 1:
	case 2:
	case 3:
	default:
		fmt.Println("mode должен быть указан 1-2-3")
		os.Exit(1)
	}
	uas, err = ReadFileToSlice(ualist)
	if err != nil {
		log.Fatal(err)
	}
	proxys, err = ReadFileToSlice(proxylist)
	if err != nil {
		log.Fatal(err)
	}
	filenames, err = ReadFileToSlice(filenlist)
	if err != nil {
		log.Fatal(err)
	}
	paths, err = ReadFileToSlice(pathlist)
	if err != nil {
		log.Fatal(err)
	}
	domains, err = ReadFileToSlice(domainlist)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	wpool := workerpool.New(workers)
	for _, d := range domains {
		domain := d
		wpool.Submit(func() {
			switch mode {
			case 1:
				SimpleMode(domain)
			case 2:
				DirMode(domain)
			case 3:
				FullMode(domain)
			}
		})
	}
	wpool.StopWait()
}

func ReadFileToSlice(source string) ([]string, error) {
	f, err := os.Open(source)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	scanner.Split(bufio.ScanLines)
	var result []string
	for scanner.Scan() {
		result = append(result, scanner.Text())
	}
	if len(result) <= 0 {
		return nil, errors.New("File empty")
	}
	return result, nil
}

func FullMode(domain string) {
	for _, path := range paths {
		for _, filename := range filenames {
			fpath := domain + "/" + path + "/" + filename
			sizeStr, code, err := getSize(fpath)
			if err != nil {
				logger(fmt.Sprintf("%s|%s|%s", domain, filename, err.Error()), "errors.log")
				continue
			}
			logger(fmt.Sprintf("%s|%s|%s|%d", fpath, filename, sizeStr, code), outfile)
		}
	}
}

func SimpleMode(domain string) {
	for _, fname := range filenames {
		u := domain + "/" + fname
		sizeStr, code, err := getSize(u)
		if err != nil {
			logger(fmt.Sprintf("%s|%s|%s", domain, fname, err.Error()), "errors.log")
			continue
		}
		logger(fmt.Sprintf("%s|%s|%s|%d", u, fname, sizeStr, code), outfile)
	}
}

func DirMode(domain string) {
	for _, path := range paths {
		fpath := domain + "/" + path
		code, err := dirStatus(fpath)
		if err != nil {
			logger(fmt.Sprintf("%s|%s|%s", domain, path, err.Error()), "errors.log")
			continue
		}
		logger(fmt.Sprintf("%s|%s|%d", domain, path, code), outfile)
	}
}

func dirStatus(url string) (int, error) {
	dialer, err := proxy.SOCKS5("tcp", getRnd(proxys), nil, proxy.Direct)
	if err != nil {
		return 0, err
	}
	transport := &http.Transport{Dial: dialer.Dial}
	client := &http.Client{
		Transport: transport,
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return 0, err
	}
	req.Header.Set("User-Agent", getRnd(uas))
	resp, err := client.Do(req)
	if err != nil {
		return 0, err
	}

	return resp.StatusCode, nil
}

func getSize(url string) (string, int, error) {
	dialer, err := proxy.SOCKS5("tcp", getRnd(proxys), nil, proxy.Direct)
	if err != nil {
		return "", 0, err
	}
	transport := &http.Transport{Dial: dialer.Dial}
	client := &http.Client{
		Transport: transport,
	}
	req, err := http.NewRequest("HEAD", url, nil)
	if err != nil {
		return "", 0, err
	}
	req.Header.Set("User-Agent", getRnd(uas))
	resp, err := client.Do(req)
	if err != nil {
		return "", 0, err
	}

	if resp.StatusCode != http.StatusOK {
		return "", resp.StatusCode, nil
	}
	size, _ := strconv.Atoi(resp.Header.Get("Content-Length"))
	dlSize := int64(size)
	return fSize(dlSize), resp.StatusCode, nil
}

func fSize(b int64) string {
	const unit = 1000
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f %cB", float64(b)/float64(div), "kMGTPE"[exp])
}

func logger(s string, outf string) {
	f, err := os.OpenFile(outf, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	newline := ""
	if runtime.GOOS == "windows" {
		newline = "\r\n"
	} else {
		newline = "\n"
	}
	if _, err = f.WriteString(s + newline); err != nil {
		log.Fatal(err)
	}
}

func getRnd(slc []string) string {
	return slc[rand.Intn(len(slc))]
}
